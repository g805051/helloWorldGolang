package main

import (
	"context"
	"fmt"

	"github.com/open-policy-agent/opa/rego"
)

const module = `
package example.authz

import rego.v1

default allow := false

allow if {
    input.method == "GET"
    input.path == ["salary", input.subject.user]
}

allow if is_admin

is_admin if "admin" in input.subject.groups
`

func main() {
	ctx := context.TODO()

	query, err := rego.New(
		rego.Query("data.example.authz.allow"),
		rego.Module("example.rego", module),
	).PrepareForEval(ctx)

	if err != nil {
		panic(err)
	}

	for _, example := range []struct {
		pathPrefix string
		user       string
		expected   bool
		groups     []string
	}{
		{"salary", "bob", true, []string{"sales", "marketing"}},
		{"salary", "notBob", false, []string{"sales", "marketing"}},
		{"salary", "BossMan", true, []string{"admin", "sales", "marketing"}},
		{"bonus", "bob", false, []string{"sales", "marketing"}},
		{"bonus", "notBob", false, []string{"sales", "marketing"}},
		{"bonus", "BossMan", true, []string{"admin", "sales", "marketing"}},
	} {
		input := map[string]interface{}{
			"method": "GET",
			"path":   []string{example.pathPrefix, "bob"},
			"subject": map[string]interface{}{
				"user":   example.user,
				"groups": example.groups,
			},
		}

		results, err := query.Eval(ctx, rego.EvalInput(input))
		if err != nil {
			panic(err)
		}
		if actual := results.Allowed(); example.expected != actual {
			fmt.Println("User:", example.user, " Expected ", example.expected, " but got ", actual, " for ", results)
		}
	}
}
