package main

import (
	"archive/zip"
	"fmt"
	"os"
	"sync"
	"time"

	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"mime"
	"mime/multipart"
	"mime/quotedprintable"
	"net/mail"
	"strings"

	"github.com/google/uuid"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap" // Requires `sudo apt install libpcap-dev`
	"github.com/google/gopacket/reassembly"
)

func main() {
	pcapFileName := "/home/mgflax/win11/Downloads/ICMMSLA.pcap"

	var wg sync.WaitGroup
	msf := &myStreamFactory{
		wellKnownPort: 25,
		wg:            &wg,
		mutex:         &sync.Mutex{},
		DoProcessStreams: func(
			netFlow string,
			tcpFlow string,
			isOutgoing bool,
			clientStream chan []byte,
			serverStream chan []byte,
		) {
			clientScanner := bufio.NewScanner(&byteChannelReader{channel: clientStream})
			serverScanner := bufio.NewScanner(&byteChannelReader{channel: serverStream})

			wg.Add(1)
			go func() {
				defer wg.Done()
				linefeed := []byte("\n")
				buffer := []byte{}
				consumingSMTP := true
				for clientScanner.Scan() {
					line := clientScanner.Text()
					if consumingSMTP {
						if strings.ToUpper(line) != "DATA" {
							continue
						} else {
							consumingSMTP = false
						}
					} else {
						if line == "RSET" {
							log.Println("RSET")
							clear(buffer)
							consumingSMTP = true
						} else if line != "." {
							buffer = append(buffer, []byte(line)...)
							buffer = append(buffer, linefeed...)
						} else {
							ProcessMessage(buffer, netFlow, tcpFlow, isOutgoing)
							clear(buffer)
							consumingSMTP = true
						}
					}
				}
			}()

			wg.Add(1)
			go func() {
				defer wg.Done()
				for serverScanner.Scan() {
					// log.Println("netFlow:", netFlow, "; server: ", serverScanner.Text())
				}
			}()
		},
	}

	RunPcap(pcapFileName, msf)
	wg.Wait()
}

func ParseBodyPart(
	mime_data io.Reader,
	boundary string,
	archive *zip.Writer,
	logfile io.Writer,
	containerName string,
) {
	reader := multipart.NewReader(mime_data, boundary)
	if reader == nil {
		fmt.Fprintln(logfile, "Null Reader for boundary: ", boundary)
		return
	}

	for {
		new_part, err := reader.NextPart()
		if err == io.EOF {
			return
		}
		if err != nil {
			fmt.Fprintln(logfile, "Error going through the MIME parts -", err)
			return
		}

		mediaType, params, err := mime.ParseMediaType(new_part.Header.Get("Content-Type"))

		if err == nil && strings.HasPrefix(mediaType, "multipart/") {
			ParseBodyPart(new_part, params["boundary"], archive, logfile, containerName)
		} else {
			part_data, err := io.ReadAll(new_part)
			if err != nil {
				fmt.Fprintln(logfile, "Error reading MIME part data", err)
				return
			}

			content_transfer_encoding := strings.ToUpper(new_part.Header.Get("Content-Transfer-Encoding"))

			filename := params["name"]
			if filename == "" {
				filename = new_part.FileName()
			}
			if filename == "" {
				log.Println("Missing filename for mediaType", mediaType, " and params", params, " in ", containerName)
				filename = uuid.NewString()
			}

			switch content_transfer_encoding {
			case "BASE64":
				decoded_content, err := base64.StdEncoding.DecodeString(string(part_data))
				if err != nil {
					fmt.Fprintln(logfile, "Error decoding BASE64", err)
				} else {
					f, err := archive.Create(filename)
					if err != nil {
						fmt.Fprintln(logfile, "Error creating zip entry ", filename, " : ", err)
					}
					f.Write(decoded_content)
				}
			case "QUOTED-PRINTABLE":
				decoded_content, err := io.ReadAll(quotedprintable.NewReader(bytes.NewReader(part_data)))
				if err != nil {
					fmt.Fprintln(logfile, "Error decoding QUOTED-PRINTABLE", filename, " : ", err)
				} else {
					f, err := archive.Create(filename)
					if err != nil {
						fmt.Fprintln(logfile, "Error creating zip entry for quoted-printable ", filename, " : ", err)
					}
					f.Write(decoded_content)
				}

			default:
				fmt.Fprintln(logfile, "content_transfer_encoding", content_transfer_encoding)

				f, err := archive.Create(filename)
				if err != nil {
					fmt.Fprintln(logfile, "Error creating zip entry for default", filename, " : ", err)
				}
				f.Write(part_data)
			}
		}
	}
}

type MetadataJson struct {
	Header   mail.Header
	NetFlow  string
	TcpFlow  string
	Outgoing bool
}

func ProcessMessage(buffer []byte,
	netFlow string,
	tcpFlow string,
	isOutgoing bool,
) {
	m, err := mail.ReadMessage(bytes.NewReader(buffer))
	if err != nil {
		log.Fatal(netFlow, err)
	}

	msgID := m.Header.Get("X-Mms-Message-Id")

	_, params, err := mime.ParseMediaType(m.Header.Get("Content-Type"))
	if err != nil {
		log.Println("ParseMediaType", msgID, netFlow, err)
	}

	filename := msgID + "___" + uuid.NewString()
	filename = strings.ReplaceAll(filename, "\"", "")

	// Create a new zip archive.
	zipfile, err := os.Create(filename + ".zip")
	if err != nil {
		log.Fatal(err)
	}
	archive := zip.NewWriter(zipfile)

	var logBuffer bytes.Buffer
	ParseBodyPart(m.Body, params["boundary"], archive, bufio.NewWriter(&logBuffer), filename)

	if logBuffer.Len() > 0 {
		logFile, err := archive.Create("____logfile.txt")
		if err != nil {
			log.Println("Error creating zip entry ", "logfile.txt", " : ", err)
		}
		logFile.Write(logBuffer.Bytes())
		log.Println("____logfile.txt:", logBuffer.String())
	}

	if true {
		b, err := json.Marshal(MetadataJson{
			Header:   m.Header,
			NetFlow:  netFlow,
			TcpFlow:  tcpFlow,
			Outgoing: isOutgoing,
		})
		if err != nil {
			log.Fatal("Serializing header info", err)
		}

		var out bytes.Buffer
		json.Indent(&out, b, "", "\t")

		metadataFile, err := archive.Create("____metadata.json")
		if err != nil {
			log.Println("Error creating zip entry ", "metadata.json", " : ", err)
		}
		metadataFile.Write(out.Bytes())
	}

	archive.Close()
	zipfile.Close()
}

//// Only generic stuff below

// implements io.Reader
// .Read() returns data which was received over the channel
// Note that .Read() may block if the channel isn't closed and the buffer is empty
type byteChannelReader struct {
	channel chan []byte
	buffer  []byte
}

func (br *byteChannelReader) Read(p []byte) (n int, err error) {
	ok := true
	for ok && len(br.buffer) == 0 {
		br.buffer, ok = <-br.channel
	}
	copiedBytes := copy(p, br.buffer)
	br.buffer = br.buffer[copiedBytes:]

	if ok {
		return copiedBytes, nil
	} else {
		return copiedBytes, io.EOF
	}
}

// implements reassembly.AssemblerContext
type myAssemblerContext struct {
	captureInfo gopacket.CaptureInfo
}

func (mac *myAssemblerContext) GetCaptureInfo() gopacket.CaptureInfo {
	return mac.captureInfo
}

// implements reassembly.StreamFactory
// Keeps track of all active reassemblyStream instances
type myStreamFactory struct {
	wellKnownPort    layers.TCPPort
	wg               *sync.WaitGroup
	DoProcessStreams func(netFlow string, tcpFlow string, isOutgoing bool, clientStream chan []byte, serverStream chan []byte)
	mutex            *sync.Mutex
	streamMap        map[string]reassemblyStream
}

func (msf *myStreamFactory) New(
	netFlow gopacket.Flow,
	tcpFlow gopacket.Flow,
	tcp *layers.TCP,
	ac reassembly.AssemblerContext,
) reassembly.Stream {
	// Number of messages to queue
	streamBufferSize := 1 << 8

	clientStream := make(chan []byte, streamBufferSize)
	serverStream := make(chan []byte, streamBufferSize)
	uuid := uuid.NewString()
	isOutgoing := tcp.DstPort == msf.wellKnownPort

	thisReassemblyStream := reassemblyStream{
		parent:        msf,
		netFlow:       &netFlow,
		tcpFlow:       &tcpFlow,
		tcpstate:      reassembly.NewTCPSimpleFSM(reassembly.TCPSimpleFSMOptions{SupportMissingEstablishment: true}),
		optionChecker: reassembly.NewTCPOptionCheck(),
		isOutgoing:    isOutgoing,
		clientStream:  clientStream,
		serverStream:  serverStream,
		uuid:          uuid,
		DoReassembledSG: func(sg reassembly.ScatterGather, ac reassembly.AssemblerContext) {
			availableBytes, _ := sg.Lengths()
			if availableBytes == 0 {
				return
			}

			direction, _, _, _ := sg.Info()
			if (direction == reassembly.TCPDirClientToServer) == isOutgoing {
				clientStream <- sg.Fetch(availableBytes)
			} else {
				serverStream <- sg.Fetch(availableBytes)
			}
		},
		DoReassemblyComplete: func(ac reassembly.AssemblerContext) bool {
			close(clientStream)
			close(serverStream)

			if msf.streamMap != nil {
				msf.mutex.Lock()
				delete(msf.streamMap, uuid)
				msf.mutex.Unlock()
			}

			return true
		},
	}

	msf.mutex.Lock()
	if msf.streamMap == nil {
		msf.streamMap = make(map[string]reassemblyStream)
	}
	msf.streamMap[thisReassemblyStream.uuid] = thisReassemblyStream
	msf.mutex.Unlock()

	msf.DoProcessStreams(netFlow.String(), tcpFlow.String(), isOutgoing, clientStream, serverStream)

	return &thisReassemblyStream
}

func RunPcap(pcapFileName string, msf *myStreamFactory) {
	count := 0
	bytes := 0

	handle, err := pcap.OpenOffline(pcapFileName)
	if err != nil {
		panic(err)
	}
	reassembler := reassembly.NewAssembler(reassembly.NewStreamPool(msf))
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		// fmt.Print(packet.Metadata().Timestamp)
		if packet == nil {
			log.Println("NIL PACKET")
			continue
		}
		if packet.NetworkLayer() == nil {
			log.Println("NIL NetworkLayer")
			continue
		}
		if packet.TransportLayer() == nil {
			log.Println("NIL TransportLayer")
			continue
		}
		if packet.TransportLayer().LayerType() != layers.LayerTypeTCP {
			log.Println("Unexpected LayerType", packet.TransportLayer().LayerType())
			continue
		}

		tcp := packet.Layer(layers.LayerTypeTCP)

		if tcp == nil {
			log.Println("NIL TCP LayerType")
			continue
		}

		tcpLayer, ok := tcp.(*layers.TCP)
		if !ok {
			log.Println("TCP is not tcp???")
			continue
		}

		if tcpLayer.SrcPort != msf.wellKnownPort && tcpLayer.DstPort != msf.wellKnownPort {
			log.Println("Src ", tcpLayer.SrcPort, " and Dst ", tcpLayer.DstPort)
			continue
		}

		assemblerContext := myAssemblerContext{captureInfo: packet.Metadata().CaptureInfo}
		reassembler.AssembleWithContext(packet.NetworkLayer().NetworkFlow(), tcpLayer, &assemblerContext)

		bytes += packet.Metadata().CaptureLength
		count++
	}
	log.Println("PacketCount:", count)
	log.Println("PacketBytes:", bytes)

	time.Sleep(time.Second)

	msf.mutex.Lock()
	for _, s := range msf.streamMap {
		close(s.clientStream)
		close(s.serverStream)
	}
	msf.mutex.Unlock()
}

// implements reassembly.Stream
type reassemblyStream struct {
	parent               *myStreamFactory
	netFlow              *gopacket.Flow
	tcpFlow              *gopacket.Flow
	tcpstate             *reassembly.TCPSimpleFSM
	optionChecker        reassembly.TCPOptionCheck
	isOutgoing           bool
	clientStream         chan []byte
	serverStream         chan []byte
	uuid                 string
	DoReassemblyComplete func(ac reassembly.AssemblerContext) bool
	DoReassembledSG      func(sg reassembly.ScatterGather, ac reassembly.AssemblerContext)
}

func (ms *reassemblyStream) Accept(tcp *layers.TCP, ci gopacket.CaptureInfo, dir reassembly.TCPFlowDirection, nextSeq reassembly.Sequence, start *bool, ac reassembly.AssemblerContext) bool {
	// if err := ms.optionChecker.Accept(tcp, ci, dir, nextSeq, start); err != nil { return false }
	return ms.tcpstate.CheckState(tcp, dir)
}

func (ms *reassemblyStream) ReassembledSG(sg reassembly.ScatterGather, ac reassembly.AssemblerContext) {
	ms.DoReassembledSG(sg, ac)
}

func (ms *reassemblyStream) ReassemblyComplete(ac reassembly.AssemblerContext) bool {
	return ms.DoReassemblyComplete(ac)
}
