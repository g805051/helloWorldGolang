This directory contains 3445 MMS messages recorded between 2024-03-06T14:32:02-0500 and 2024-03-06T14:32:53 on a single machine.  

Each message has a zip file.  The first part of the name is the X-Mms-Message-Id, and after the ___ is a UUID (just because there are instances in which two messages share an X-Mms-Message-Id.

We use zip files because a multimedia email may contain an arbitrary number of attachments. 

In addition to the actual attachments, each zip archive also contains the SMTP headers in a file called "____metadata.json". 

If you also find a file called "____logfile.txt", then something unusual happened with the message and I'd be curious to see that instance.

Not all messages contain images, and not all images have a convenient `.jpg` (or similar) suffix -- but looking at the largest archives first is a valid strategy to find image attachments.

Many of these messages are actual P2P MMS messages, so treat them with the appropriate care.
