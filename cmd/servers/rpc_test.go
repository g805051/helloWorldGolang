package main

import (
	"context"
	"log"
	"net"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
	hello "syniverse.com/playground/pkg/hello"
	protos "syniverse.com/playground/pkg/protos"
)

func TestHelloService(t *testing.T) {
	testCases := []struct {
		input  string
		output string
	}{
		{input: `{"name":"Cody","multiplicity":3}`, output: `{"Messages":["Hello, Cody","Hello, Cody","Hello, Cody"]}`}}

	ctx := context.Background()

	listener := bufconn.Listen(1024 * 1024)
	StartRPCServer(listener)

	conn, err := grpc.DialContext(ctx, "", grpc.WithInsecure(),
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) { return listener.Dial() }))
	if err != nil {
		log.Fatal(err)
	}

	for _, tt := range testCases {
		t.Run(tt.input, func(t *testing.T) {
			req, err := hello.DeserializeRequest(tt.input)
			if err != nil {
				t.Error("Unable to deserialize test case request", err)
				return
			}

			response, err := protos.NewHelloServiceClient(conn).Process(ctx, req)
			if err != nil {
				t.Error("Unable to process test case request", err)
				return
			}

			responseJson, err := hello.SerializeResponse(response)
			if err != nil {
				t.Error("Unable to serialize test case response", err)
				return
			}

			if responseJson != tt.output {
				t.Error("error: expected", tt.output, "received", responseJson)
				return
			}
		})
	}
}
