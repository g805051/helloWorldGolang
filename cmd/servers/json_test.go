package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestHelloRoute(t *testing.T) {
	testCases := []struct {
		input  string
		output string
	}{
		{input: `{"name":"Cody","multiplicity":3}`, output: `{"Messages":["Hello, Cody","Hello, Cody","Hello, Cody"]}`},
		{input: `{"name":"Cody","multiplicity":0}`, output: `{"Messages":[]}`}}

	router := gin.Default()
	setupRouter(router)

	for _, testCase := range testCases {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", "/hello", bytes.NewBufferString(testCase.input))
		router.ServeHTTP(w, req)

		assert.Equal(t, 200, w.Code)
		assert.Equal(t, testCase.output, w.Body.String())
	}
}
