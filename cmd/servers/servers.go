package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"sync"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	hello "syniverse.com/playground/pkg/hello"
	"syniverse.com/playground/pkg/protos"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

type Server struct {
	protos.UnimplementedHelloServiceServer
}

func setupRouter(r *gin.Engine) {
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	r.POST("/hello", func(c *gin.Context) {
		jsonData, err := io.ReadAll(c.Request.Body)
		if err != nil {
			c.String(500, "Deserialization error: "+err.Error())
			return
		}

		jsonResponse, err := hello.ProcessJson(string(jsonData))
		if err != nil {
			c.String(500, "Processing error: "+err.Error())
			return
		}

		c.String(200, jsonResponse)
	})
}

func (s *Server) Process(ctx context.Context, in *protos.HelloRequest) (*protos.HelloResponse, error) {
	log.Printf("Received: %v", in)
	response := hello.Process(in)
	return response, nil
}

func StartRPCServer(listener net.Listener) {
	server := grpc.NewServer()
	protos.RegisterHelloServiceServer(server, &Server{})

	healthcheck := health.NewServer()
	grpc_health_v1.RegisterHealthServer(server, healthcheck)

	log.Printf("server listening at %v", listener.Addr())

	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()
}

func main() {
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()

		r := gin.Default()
		setupRouter(r)
		r.Run(":8080")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()

		flag.Parse()
		listener, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		StartRPCServer(listener)
	}()

	wg.Wait()
}
