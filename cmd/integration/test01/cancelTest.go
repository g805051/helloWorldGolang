package main

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"

	hello "syniverse.com/playground/pkg/hello"
	"syniverse.com/playground/pkg/protos"
)

type GetBinding struct {
	Memo   string `uri:"memo" binding:"required"`
	Millis int    `uri:"millis" binding:"required"`
}

func makeRouter(zerolog zerolog.Logger) *gin.Engine {
	r := gin.Default()

	r.GET("/delay/:memo/:millis",
		func(c *gin.Context) {
			deadline, ok := c.Deadline()
			log.Info().Time("now", time.Now()).Time("deadline", deadline).Bool("ok", ok).Msg("Http Server Context")

			var getBinding GetBinding
			if err := c.ShouldBindUri(&getBinding); err != nil {
				c.JSON(400, gin.H{"msg": err})
				return
			}

			zerolog.Info().Str("Memo", getBinding.Memo).Int("Millis", getBinding.Millis).Msg("Http Server Before")
			time.Sleep(time.Millisecond * time.Duration(getBinding.Millis))
			zerolog.Info().Str("Memo", getBinding.Memo).Int("Millis", getBinding.Millis).Msg("Http Server After")
			c.String(200, "pong: "+getBinding.Memo)
		})
	return r
}

var mutex sync.Mutex

func curl(ctx context.Context, httpEndpoint string, memo string, millis int) (s string, err error) {
	mutex.Lock()
	defer mutex.Unlock()

	deadline, ok := ctx.Deadline()

	log.Info().Time("now", time.Now()).Time("deadline", deadline).Bool("ok", ok).Str("memo", memo).Int("millis", millis).Any("Hail", ctx.Value("Hail")).Msg("Client Before")

	treq, err := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("%s/%s/%d", httpEndpoint, memo, millis), nil)
	if err != nil {
		log.Info().Err(err).Str("response", s).Str("memo", memo).Int("millis", millis).Msg("NewRequestWIthContext")
		return
	}

	tres, err := http.DefaultClient.Do(treq)
	if err != nil {
		log.Info().Err(err).Str("response", s).Str("memo", memo).Int("millis", millis).Msg("Do")
		return
	}

	bytes, err := io.ReadAll(tres.Body)
	if err != nil {
		log.Info().Err(err).Str("response", s).Str("memo", memo).Int("millis", millis).Msg("ReadAll")
		return
	}

	s = string(bytes)
	log.Info().AnErr("error", err).Str("response", s).Str("memo", memo).Int("millis", millis).Msg("Client After")
	return
}

type HelloServer struct {
	protos.UnimplementedHelloServiceServer
}

const grpcPort = 9000

func (s *HelloServer) Process(ctx context.Context, in *protos.HelloRequest) (*protos.HelloResponse, error) {
	deadline, ok := ctx.Deadline()
	log.Info().Time("now", time.Now()).Time("deadline", deadline).Bool("ok", ok).Str("in", in.String()).Any("Hail", ctx.Value("Hail")).Msg("GRPC Server")
	md, ok := metadata.FromIncomingContext(ctx)
	log.Info().Time("now", time.Now()).Bool("ok", ok).Any("md", md).Msg("FromIncomingContext")
	vals := md.Get("key1")
	log.Info().Any("vals", vals).Msg("FromIncomingContext")
	response := hello.Process(in)
	return response, nil
}

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.TimeFieldFormat = time.RFC3339Nano

	httpRouter := makeRouter(log.Logger)
	go func() { httpRouter.Run(":8080") }()

	go func() {
		grpcServer := grpc.NewServer()
		protos.RegisterHelloServiceServer(grpcServer, &HelloServer{})

		listener, err := net.Listen("tcp", fmt.Sprintf(":%d", grpcPort))
		if err != nil {
			log.Fatal().Err(err).Int("grpcPort", grpcPort).Msg("Creating serving listener")
		}
		if err := grpcServer.Serve(listener); err != nil {
			log.Fatal().Err(err).Int("grpcPort", grpcPort).Msg("Running grpc server")
		}
	}()

	time.Sleep(time.Millisecond)

	httpEndpoint := "http://localhost:8080/delay"

	ctx, _ := context.WithTimeout(context.Background(), 10000*time.Millisecond)
	go func() { curl(ctx, httpEndpoint, "Wait500", 500) }()
	go func() { curl(ctx, httpEndpoint, "Wait1000", 1000) }()
	go func() { curl(ctx, httpEndpoint, "Wait2000", 2000) }()

	ctx, _ = context.WithTimeout(context.WithValue(context.Background(), "Hail", "Eris"), time.Hour)
	ctx = metadata.NewOutgoingContext(ctx, metadata.Pairs("key1", "val1"))

	deadline, ok := ctx.Deadline()
	log.Info().Time("now", time.Now()).Time("deadline", deadline).Bool("ok", ok).
		Any("Hail", ctx.Value("Hail")).
		Msg("CLIENT DEADLINE")

	grpcEndpoint := fmt.Sprintf("localhost:%d", grpcPort)

	clientConn, err := grpc.Dial(grpcEndpoint, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal().Err(err).Str("grpcEndpoint", grpcEndpoint).Msg("Creating client listener")
	}

	grpcClient := protos.NewHelloServiceClient(clientConn)
	r, err := grpcClient.Process(ctx, &protos.HelloRequest{Name: "Hezi", Multiplicity: 4})
	if err != nil {
		log.Fatal().Err(err).Str("grpcEndpoint", grpcEndpoint).Msg("GRPC Client")
	}

	log.Info().Str("grpcResponse", r.String()).Msg("Response")
}
