package types

import (
	"testing"

	"syniverse.com/playground/pkg/protos"
)

func TestFailures(t *testing.T) {
	garbageJson := "}}}"

	_, err := DeserializeRequest(garbageJson)
	if err == nil {
		t.Errorf("Expected an error when deserializing garbage json: %s", garbageJson)
	}

	_, err = ProcessJson(garbageJson)
	if err == nil {
		t.Errorf("Expected an error when processing garbage input: %s", garbageJson)
	}
}

func TestProcessJson(t *testing.T) {
	testCases := []struct {
		input  string
		output string
	}{
		{input: `{"name":"Cody","multiplicity":3}`, output: `{"Messages":["Hello, Cody","Hello, Cody","Hello, Cody"]}`}}
	for _, testCase := range testCases {
		computedResponse, err := ProcessJson(testCase.input)

		if err != nil {
			t.Errorf("Error %v while processing %s", err, testCase.input)
			continue
		}

		if computedResponse != testCase.output {
			t.Errorf("Got %s but expected %s for %s", computedResponse, testCase.output, testCase.input)
		}

	}
}

func TestProcess(t *testing.T) {
	testCases := []struct {
		name           string
		multiplicity   int32
		expectedCount  int
		expectedString string
	}{
		{name: "Marshall", multiplicity: 2, expectedCount: 2, expectedString: "Hello, Marshall"},
		{name: "Marshall", multiplicity: 3, expectedCount: 3, expectedString: "Hello, Marshall"},
		{name: "Cody", multiplicity: 3, expectedCount: 3, expectedString: "Hello, Cody"},
	}

	for _, testCase := range testCases {
		request := protos.HelloRequest{
			Name:         testCase.name,
			Multiplicity: testCase.multiplicity}
		response := Process(&request)

		if messageCount := len(response.Messages); messageCount != testCase.expectedCount {
			t.Errorf("Got %d messages, expected %d", messageCount, testCase.expectedCount)
		}

		if firstMessage := response.Messages[0]; firstMessage != testCase.expectedString {
			t.Errorf("Got %s first message, expected %s", firstMessage, testCase.expectedString)
		}
	}
}

func TestSerializeAndDeserialize(t *testing.T) {
	testCases := []struct {
		name           string
		multiplicity   int32
		expectedString string
	}{
		{name: "Marshall", multiplicity: 2, expectedString: `{"Name":"Marshall","Multiplicity":2}`},
	}
	for _, testCase := range testCases {
		request := protos.HelloRequest{
			Name:         testCase.name,
			Multiplicity: testCase.multiplicity}

		serialized, err := SerializeRequest(&request)
		if err != nil {
			t.Errorf("Error %v but expected %s", err, testCase.expectedString)
			continue
		}
		if serialized != testCase.expectedString {
			t.Errorf("Serialized to %s, expected %s", serialized, testCase.expectedString)

		}
	}
	for _, testCase := range testCases {
		expectedRequest := protos.HelloRequest{
			Name:         testCase.name,
			Multiplicity: testCase.multiplicity}

		deserializedRequest, err := DeserializeRequest(testCase.expectedString)

		if err != nil {
			t.Errorf("Error %v while parsing %s", err, testCase.expectedString)
			continue
		}
		if deserializedRequest.Name != expectedRequest.Name {
			t.Errorf("Serialized to %v, expected %v", deserializedRequest, expectedRequest)
		}
		if deserializedRequest.Multiplicity != expectedRequest.Multiplicity {
			t.Errorf("Serialized to %v, expected %v", deserializedRequest, expectedRequest)
		}
	}

}
