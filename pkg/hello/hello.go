package types

import (
	"encoding/json"
	"fmt"

	"syniverse.com/playground/pkg/protos"
)

func Process(req *protos.HelloRequest) *protos.HelloResponse {
	msgs := make([]string, 0, req.Multiplicity)
	for i := int32(0); i < req.Multiplicity; i++ {
		msgs = append(msgs, fmt.Sprintf("Hello, %s", req.Name))
	}
	return &protos.HelloResponse{Messages: msgs}
}

func ProcessJson(requestJson string) (string, error) {
	req, err := DeserializeRequest(requestJson)

	if err != nil {
		return "", err
	}

	response := Process(req)

	return SerializeResponse(response)
}

func SerializeRequest(req *protos.HelloRequest) (string, error) {
	bytes, err := json.Marshal(req)

	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

func SerializeResponse(resp *protos.HelloResponse) (string, error) {
	bytes, err := json.Marshal(resp)

	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

func DeserializeRequest(s string) (*protos.HelloRequest, error) {
	var result protos.HelloRequest
	err := json.Unmarshal([]byte(s), &result)
	return &result, err
}
