// Package collect runs a number of goroutines and returns the first good response, or the last bad response.
//
// This is useful for reducing tail latency in a distributed system.
package collect

import (
	"context"
)

// A simple function with arbitrary inputs and outputs.
// Note that context cancellation only works if each supplied function respects ctx.Done() events.
type Collectable[I any, V any] func(ctx context.Context, input I) (output V, err error)

// A simple function with no inputs (except context) and arbitrary outputs.
// Note that context cancellation only works if each supplied function respects ctx.Done() events.
type CollectableClosure[V any] func(ctx context.Context) (output V, err error)

// Tuples are not first-class Golang entities. In particular, a channel cannot accept a
// (value,error) tuple without being wrapped in a struct.
type Result[V any] struct {
	Val V
	Err error
}
