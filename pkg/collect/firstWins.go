// Package collect runs a number of goroutines and returns the first good response, or the last bad response.
//
// This is useful for reducing tail latency in a distributed system.
package collect

import (
	"context"
	"sync"
)

// Run any number of functions, each in its own goroutine. The first to respond with a non-error wins and
// the others are cancelled. If all routines return errors, return the last one.
// 
// This function returns before the above goroutines complete.
//
// If you want this function to have a time-limit, then pass in a context with timeout and ensure that your
// functions respect ctx->Done().
func FirstFunctionWins[I any, V any](
	ctx context.Context,
	lambdas []Collectable[I, V], input I) (
	result <-chan Result[V],
	cancel context.CancelFunc,
	ok func() bool) {
	responses := make(chan Result[V], len(lambdas))

	ctx, cancel = context.WithCancel(ctx)

	var _counter sync.Mutex
	counter := len(lambdas)

	for _, lambda := range lambdas {
		go func(f Collectable[I, V]) {
			out, err := f(ctx, input)

			_counter.Lock()
			counter--
			lastChance := counter == 0
			_counter.Unlock()

			if err == nil || lastChance {
				response := Result[V]{out, err}
				select {
				case responses <- response:
					cancel()
				default:
					panic("Unable to write response")
				}
			}
		}(lambda)
	}

	return responses, cancel, func() bool {
		_counter.Lock()
		defer _counter.Unlock()
		return counter == 0
	}

}

func FirstClosureWins[V any](ctx context.Context, lambdas []CollectableClosure[V]) (result <-chan Result[V], cancel context.CancelFunc, done func() bool) {
	responses := make(chan Result[V], len(lambdas))

	ctx, cancel = context.WithCancel(ctx)

	var _counter sync.Mutex
	counter := len(lambdas)

	for _, lambda := range lambdas {
		go func(f CollectableClosure[V]) {
			out, err := f(ctx)

			_counter.Lock()
			counter--
			lastChance := counter == 0
			_counter.Unlock()

			if err == nil || lastChance {
				response := Result[V]{out, err}
				select {
				case responses <- response:
					cancel()
				default:
					panic("Unable to write response")
				}
			}
		}(lambda)
	}

	return responses, cancel, func() bool {
		_counter.Lock()
		defer _counter.Unlock()
		return counter == 0
	}
}
