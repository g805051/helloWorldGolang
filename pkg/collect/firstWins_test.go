package collect

import (
	"context"
	"errors"
	"testing"
	"time"
)

func TestSingleThreaded(t *testing.T) {
	var lambdas []Collectable[string, string]

	c1 := make(chan bool)
	lambdas = append(lambdas,
		func(ctx context.Context, s string) (string, error) {
			<-c1
			return s + "!", nil
		})

	ctx := context.Background()
	resultChannel, _, isDoneCallback := FirstFunctionWins(ctx, lambdas, "Hello")

	// Finally unblock the above code
	c1 <- true

	// Might have to wait here for the result to actually be available
	result := <-resultChannel

	if result.Err != nil {
		t.Errorf("Unexpected Error %s", result.Err)
	}

	if result.Val != "Hello!" {
		t.Errorf("Got %s but expected %s", result.Val, "Hello!")
	}

	if !isDoneCallback() {
		t.Errorf("Collector not done")
	}
}

func TestReverseOrder(t *testing.T) {
	c1 := make(chan bool, 1)
	c2 := make(chan bool, 1)

	lambdas := []Collectable[string, string]{
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c1:
				return s + "!", nil
			case <-ctx.Done():
				return "", ctx.Err()
			}
		},
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c2:
				return s + "?", nil
			case <-ctx.Done():
				return "", ctx.Err()
			}
		}}

	ctx := context.Background()
	resultChannel, _, isDoneCallback := FirstFunctionWins(ctx, lambdas, "Hello")

	// Finally unblock the above code, but in reverse order
	c2 <- true
	time.Sleep(time.Millisecond * 1)
	c1 <- true

	// Might have to wait here for the result to actually be available
	result := <-resultChannel

	if result.Err != nil {
		t.Errorf("Unexpected Error %s", result.Err)
	}

	if expected := "Hello?"; result.Val != expected {
		t.Errorf("Got %s but expected %s", result.Val, expected)
	}

	// Give time for the other threads to complete
	time.Sleep(time.Millisecond * 1)

	if !isDoneCallback() {
		t.Errorf("Collector not done")
	}
}

func TestCancelOne(t *testing.T) {
	c1 := make(chan bool, 1)
	c2 := make(chan bool, 1)

	lambdas := []Collectable[string, string]{
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c1:
				return s + "!", nil
			case <-ctx.Done():
				return "", ctx.Err()
			}
		},
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c2:
				return s + "?", nil
			case <-ctx.Done():
				return "", ctx.Err()
			}
		}}

	ctx := context.Background()
	resultChannel, _, isDoneCallback := FirstFunctionWins(ctx, lambdas, "Hello")

	// Finally unblock the above code, but in reverse order
	c2 <- true
	// Don't unblock the first lambda, and so let it be cancelled automatically
	// c1 <- true

	// Might have to wait here for the result to actually be available
	result := <-resultChannel

	if result.Err != nil {
		t.Errorf("Unexpected Error %s", result.Err)
	}

	if expected := "Hello?"; result.Val != expected {
		t.Errorf("Got %s but expected %s", result.Val, expected)
	}

	// Give time for the other threads to complete
	time.Sleep(time.Millisecond * 1)

	if !isDoneCallback() {
		t.Errorf("Collector not done")
	}
}

func TestErrorFirst(t *testing.T) {
	c1 := make(chan bool, 1)
	c2 := make(chan bool, 1)

	c2 <- true

	lambdas := []Collectable[string, string]{
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c1:
				return s + "!", nil
			case <-ctx.Done():
				return "", ctx.Err()
			}
		},
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c2:
				return "", errors.New("Something")
			case <-ctx.Done():
				return "", ctx.Err()
			}
		}}

	ctx := context.Background()
	resultChannel, _, isDoneCallback := FirstFunctionWins(ctx, lambdas, "Hello")

	time.Sleep(time.Millisecond * 1)

	c1 <- true

	result := <-resultChannel

	if result.Err != nil {
		t.Errorf("Unexpected Error %s", result.Err)
	}

	if expected := "Hello!"; result.Val != expected {
		t.Errorf("Got %s but expected %s", result.Val, expected)
	}

	// Give time for the other threads to complete
	time.Sleep(time.Millisecond * 1)

	if !isDoneCallback() {
		t.Errorf("Collector not done")
	}
}

func TestBlocked(t *testing.T) {
	c1 := make(chan bool, 1)
	c2 := make(chan bool, 1)

	lambdas := []Collectable[string, string]{
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c1:
				return s + "!", nil
			case <-ctx.Done():
				return "", ctx.Err()
			}
		},
		func(ctx context.Context, s string) (string, error) {
			select {
			case <-c2:
				return "", errors.New("Something")
			case <-ctx.Done():
				return "", ctx.Err()
			}
		}}

	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond*5)
	resultChannel, _, isDoneCallback := FirstFunctionWins(ctx, lambdas, "Hello")
	result := <-resultChannel
	if result.Err.Error() != "context deadline exceeded" {
		t.Errorf("Unexpected Error %s", result.Err)
	}
	if !isDoneCallback() {
		t.Errorf("Collector not done")
	}
}
