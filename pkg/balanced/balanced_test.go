package balanced

import (
	"fmt"
	"sort"
	"strings"
	"testing"
)

// Really, this should be some sort of union:
// - If isLeaf==true, then only value should be populated
// - If isLeaf==false, then only open, close, and children should be populated.
type Tree[T any] struct {
	isLeaf   bool
	open     rune
	close    rune
	value    T
	children []Tree[T]
}

func parse(s string) (Tree[rune], error) {
	currentLevel := Tree[rune]{isLeaf: false}
	stack := []Tree[rune]{}
	for _, c := range s {
		switch c {
		case '(', '[', '{':
			stack = append(stack, currentLevel)
			switch c {
			case '[':
				currentLevel = Tree[rune]{isLeaf: false, open: c, close: ']'}
			case '{':
				currentLevel = Tree[rune]{isLeaf: false, open: c, close: '}'}
			case '(':
				currentLevel = Tree[rune]{isLeaf: false, open: c, close: ')'}
			}
		case ')', ']', '}':
			if c != currentLevel.close {
				return currentLevel, fmt.Errorf("Got %v expected %v", c, currentLevel.close)
			}
			parent := stack[len(stack)-1]
			parent.children = append(parent.children, currentLevel)
			currentLevel = parent
			stack = stack[:len(stack)-1]
		default:
			currentLevel.children = append(currentLevel.children, Tree[rune]{isLeaf: true, value: c})
		}
	}
	if len(stack) > 0 {
		return currentLevel, fmt.Errorf("Missing closes in %v", stack)
	}
	return currentLevel, nil
}

func toSortedString(this Tree[rune]) string {
	if this.isLeaf {
		return string(this.value)
	}
	substrings := []string{}
	for _, node := range this.children {
		substrings = append(substrings, toSortedString(node))
	}

	sort.Slice(substrings,
		func(i int, j int) bool {
			return substrings[i] < substrings[j]
		})

	// Top level doesn't have open/close
	if this.open == 0 {
		return strings.Join(substrings, "")
	}

	return string(this.open) + strings.Join(substrings, "") + string(this.close)
}

func Test1(t *testing.T) {
	examples := []string{
		"TS{[YX][S[[BA]]E[DC]E]}S",
	}

	for _, example := range examples {
		tree, err := parse(example)
		if err != nil {
			t.Fatal(err)
		}
		canonical := toSortedString(tree)
		t.Errorf("%s %v", example, canonical)
	}
}

// func Test1(t *testing.T) {
// 	table := []struct {
// 		s1      string
// 		s2      string
// 		similar bool
// 	}{
// 		{"A", "A", true},
// 		{"A", "B", false},
// 	}

// 	for _, scenario := range table {
// 		if expected, actual := scenario.similar, isSimilar(scenario.s1, scenario.s2); expected != actual {
// 			t.Fatal("Expected", expected, "actual", actual, "for", scenario)
// 		}
// 	}
// }
