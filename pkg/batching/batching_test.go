package batching

import (
	"fmt"
	"testing"
	"time"
)

func intToString(vals []int) ([]string, error) {
	result := []string{}
	for _, val := range vals {
		result = append(result, fmt.Sprintf("%d", val))
	}
	return result, nil
}

func TestWaitSecond(t *testing.T) {
	batchProcessor := CreateBatchProcessor(2, time.Second, intToString)

	result1 := make(chan Response[string], 1)
	batchProcessor <- RequestEntry[int, string]{requestVal: 0, responseChan: &result1}
	answer := <-result1
	if answer.err != nil {
		t.Fatal(answer.err)
	}
	if answer.val != "0" {
		t.Fatal("Expected", "0", "got", answer.val)
	}
}

func TestFullBatch(t *testing.T) {
	batchProcessor := CreateBatchProcessor(2, time.Hour, intToString)

	result0 := make(chan Response[string], 1)
	batchProcessor <- RequestEntry[int, string]{requestVal: 0, responseChan: &result0}
	result1 := make(chan Response[string], 1)
	batchProcessor <- RequestEntry[int, string]{requestVal: 1, responseChan: &result1}
	answer0, ok0 := <-result0
	answer1, ok1 := <-result1
	if answer0.err != nil {
		t.Fatal(answer0.err)
	}
	if answer1.err != nil {
		t.Fatal(answer1.err)
	}
	if !ok0 {
		t.Fatal("Expected ok0")
	}
	if !ok1 {
		t.Fatal("Expected ok0")
	}
	if answer0.val != "0" {
		t.Fatal("Expected", "0", "got", answer0.val)
	}
	if answer1.val != "1" {
		t.Fatal("Expected", "1", "got", answer1.val)
	}
}
