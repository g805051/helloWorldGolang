package batching

import (
	"time"
)

type Response[S any] struct {
	val S
	err error
}

type RequestEntry[T any, S any] struct {
	requestVal   T
	responseChan *chan Response[S]
}

// Inputs:
// - maxBatchSize   -- largest size of slice to process at once
// - timeOut        -- max time a query can remain before being processed
// - batchProcessor -- transforms a slice of T into a slice of S
// Output:
// - A queue into which one can deposit requests.  Each request is a structure containing
//   - A requestVal (of type T)
//   - A responseChan (of type Response[S], i.e. a val of type S and an error) into which the response will be written
func CreateBatchProcessor[T any, S any](
	maxBatchSize int,
	timeout time.Duration,
	batchProcessor func([]T) ([]S, error),
) chan RequestEntry[T, S] {
	batchingChan := make(chan RequestEntry[T, S])
	requests := make([]T, 0, maxBatchSize)
	responseChannels := make([]*chan Response[S], 0, maxBatchSize)
	var timeOutChan <-chan time.Time

	doBatch := func() {
		responses, err := batchProcessor(requests)
		for idx, resp := range responses {
			select {
			case *responseChannels[idx] <- Response[S]{val: resp, err: err}:
				close(*responseChannels[idx])
			default:
			}
		}

		clear(responseChannels)
		clear(requests)
		timeOutChan = nil
	}

	go func() {
		for {
			select {
			case batchRequest := <-batchingChan:
				requests = append(requests, batchRequest.requestVal)
				responseChannels = append(responseChannels, batchRequest.responseChan)

				if timeOutChan == nil {
					timeOutChan = time.After(timeout)
				}

				if len(responseChannels) >= maxBatchSize {
					doBatch()
				}

			case <-timeOutChan:
				doBatch()
			}
		}
	}()

	return batchingChan
}
