package httpclient

import (
	"context"
	"errors"
	"io"
	"net/http"
)

func HttpGetter(url string) func(ctx context.Context) (string, error) {
	return func(ctx context.Context) (string, error) {
		const nilValue = ""
		req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
		if err != nil {
			return nilValue, err
		}

		rid := ctx.Value("request_id")
		if rid != nil {
			req.Header.Add("X-Request-Id", rid.(string))
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nilValue, err
		}
		defer resp.Body.Close()

		if resp.StatusCode == http.StatusNotFound {
			return nilValue, errors.New("URL not found")
		}

		if resp.StatusCode != http.StatusOK {
			return nilValue, errors.New("Server not OK")
		}

		bytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return nilValue, err
		}

		return string(bytes), nil
	}
}
