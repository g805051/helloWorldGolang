package httpclient

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"syniverse.com/playground/pkg/collect"
)

func Test1(t *testing.T) {
	body := `Hello world!`

	server1 := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) { rw.Write([]byte(body)) }))
	defer server1.Close()

	closures := []collect.CollectableClosure[string]{HttpGetter(server1.URL)}

	ctx := context.Background()
	ctx = context.WithValue(ctx, "request_id", "123456789")
	resultChannel, _, _ := collect.FirstClosureWins(ctx, closures)

	result := <-resultChannel

	if result.Err != nil {
		t.Errorf("Got unexpected error err")
	}
	if result.Val != body {
		t.Errorf("Got body %s, expected %s", result.Val, body)
	}
}

// Test in which we talk simultaneously to two http servers, the first one of which is blocked
func Test2(t *testing.T) {
	body := `Hello world!`

	c1 := make(chan bool, 1)
	// c2 := make(chan bool, 1)

	server1 := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) { <-c1; rw.Write([]byte(body)) }))
	server2 := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) { rw.Write([]byte(body)) }))
	closures := []collect.CollectableClosure[string]{
		HttpGetter(server1.URL),
		HttpGetter(server2.URL),
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond*10)

	resultChannel, _, _ := collect.FirstClosureWins(ctx, closures)

	result := <-resultChannel

	if result.Err != nil {
		t.Errorf("Got unexpected error %s", result.Err.Error())
	}
	if result.Val != body {
		t.Errorf("Got body %s, expected %s", result.Val, body)
	}
}

func Test404Error(t *testing.T) {
	server1 := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) { rw.WriteHeader(404) }))
	closures := []collect.CollectableClosure[string]{HttpGetter(server1.URL)}

	resultChannel, _, _ := collect.FirstClosureWins(context.Background(), closures)
	result := <-resultChannel

	if expected := "URL not found"; result.Err.Error() != expected {
		t.Errorf("Got unexpected error %s, expected %s", result.Err.Error(), expected)
	}
}

func Test500Error(t *testing.T) {
	server1 := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) { rw.WriteHeader(500) }))
	closures := []collect.CollectableClosure[string]{HttpGetter(server1.URL)}

	resultChannel, _, _ := collect.FirstClosureWins(context.Background(), closures)
	result := <-resultChannel

	if expected := "Server not OK"; result.Err.Error() != expected {
		t.Errorf("Got unexpected error %s, expected %s", result.Err.Error(), expected)
	}
}

func TestBadURL(t *testing.T) {
	closures := []collect.CollectableClosure[string]{HttpGetter("garbageURL")}

	resultChannel, _, _ := collect.FirstClosureWins(context.Background(), closures)
	result := <-resultChannel

	if expected := `Get "garbageURL": unsupported protocol scheme ""`; result.Err.Error() != expected {
		t.Errorf("Got unexpected error %s, expected %s", result.Err.Error(), expected)
	}
}
