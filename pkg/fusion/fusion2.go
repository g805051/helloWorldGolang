// Listen to channels until a final spam/ham decision can be made.
package fusion

import (
	"context"
	"sync"
)

type State[T any] struct {
	IsStarted   bool
	IsCompleted bool
	Val         T
	Error       error
}

type StateInfos[T1 any, T2 any, T3 any, T4 any] struct {
	S1 State[T1]
	S2 State[T2]
	S3 State[T3]
	S4 State[T4]
}

type StuffToDo[T1 any, T2 any, T3 any, T4 any] struct {
	F1 func(context.Context) (T1, error)
	F2 func(context.Context) (T2, error)
	F3 func(context.Context) (T3, error)
	F4 func(context.Context) (T4, error)
}

func RunStateMachine[T1 any, T2 any, T3 any, T4 any, O any](
	ctx context.Context,
	stateLogic func(stateInfos StateInfos[T1, T2, T3, T4]) (
		done bool,
		result O,
		err error,
		stuffToDo StuffToDo[T1, T2, T3, T4],
	),
) (
	finalResult O,
	wg *sync.WaitGroup,
	finalErr error,
) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	var s StateInfos[T1, T2, T3, T4]

	c1, c2, c3, c4 := make(chan State[T1]), make(chan State[T2]), make(chan State[T3]), make(chan State[T4])

	for {
		select {
		case <-ctx.Done():
			finalErr = ctx.Err()
			return // This shouldn't happen
		case s.S1 = <-c1:
		case s.S2 = <-c2:
		case s.S3 = <-c3:
		case s.S4 = <-c4:
		}

		done, result, err, stuffToDo := stateLogic(s)
		if done {
			return result, wg, err
		}

		perhapsStartGoRoutine[T1](ctx, wg, c1, &s.S1, stuffToDo.F1)
		perhapsStartGoRoutine[T2](ctx, wg, c2, &s.S2, stuffToDo.F2)
		perhapsStartGoRoutine[T3](ctx, wg, c3, &s.S3, stuffToDo.F3)
		perhapsStartGoRoutine[T4](ctx, wg, c4, &s.S4, stuffToDo.F4)
	}
}

func perhapsStartGoRoutine[T any](
	ctx context.Context,
	wg *sync.WaitGroup,
	c chan State[T],
	s *State[T],
	f func(context.Context) (T, error),
) {
	if f == nil {
		return
	}

	s.IsStarted = true
	wg.Add(1)
	go func() {
		defer wg.Done()

		val, err := f(ctx) // This may block for a while

		select {
		case <-ctx.Done():
		case c <- State[T]{IsStarted: true, IsCompleted: true, Val: val, Error: err}:
		}
	}()
}
