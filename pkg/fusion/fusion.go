// Listen to channels until a final spam/ham decision can be made.
package fusion

import (
	"context"
	"fmt"
)

// One cannot put a (val, err) tuple onto a channel, unfortunately.
type WithError[V any] struct {
	Value V
	Err   error
}

type ReceiveStatus int8

const (
	NotStarted ReceiveStatus = iota
	Running
	Received
	Cancelled
)

// Internal state for each downstream service
type receiveState[T any] struct {
	receiveChannel   <-chan WithError[T]
	receivedResponse WithError[T]
	receiveStatus    ReceiveStatus
}

// Wrapper exposing state to the processing logic
type StateWrapper[T any] struct {
	GetStatus  func() ReceiveStatus
	GetData    func() *WithError[T]
	SetChannel func(<-chan WithError[T]) error
}

func makeWrapper[T any](state *receiveState[T]) StateWrapper[T] {
	return StateWrapper[T]{
		GetStatus: func() ReceiveStatus {
			return state.receiveStatus
		},
		GetData: func() *WithError[T] {
			return &state.receivedResponse
		},
		SetChannel: func(c <-chan WithError[T]) (err error) {
			if state.receiveStatus == NotStarted || state.receiveStatus == Cancelled {
				state.receiveChannel = c
				state.receiveStatus = Running
				return nil
			} else {
				return fmt.Errorf("illegal to start new channel when channel is currently %d", state.receiveStatus)
			}
		},
	}
}

func startState[T any](c <-chan WithError[T]) receiveState[T] {
	if c == nil {
		return receiveState[T]{
			receiveStatus: NotStarted,
		}
	} else {
		return receiveState[T]{
			receiveChannel: c,
			receiveStatus:  Running,
		}
	}
}

func acceptData[T any](state *receiveState[T], received WithError[T], ok bool) {
	state.receiveChannel = nil
	if ok {
		state.receiveStatus = Received
		state.receivedResponse = received
	} else if state.receiveStatus == Running {
		state.receiveStatus = Cancelled
	}
}

// RunStateMachine is designed to be run in its own goroutine.
//   - It will run until completion and then return a result.
//   - The caller may supply a callback specifying channels to run when the state machine starts,
//     or it may supply nil channels and allow processState to start new goroutines and
//     update the other receive states with the corresponding channels.
//   - For each data source we keep track of whether data has been received at all.
//   - Note that timed events (e.g. "return *something* after 5 seconds" can be accomplished)
//     simply by using a channel built by `time.After(time.Second * 5)` and then having the
//     processState look to see whether the corresponding "dataReceived" state.
//   - All of the business logic of course resides in initialState() and processState()
//     The logic in processState() should care about which channels have returned data and which
//     haven't -- but it shouldn't care about the order in which the channels returned data.
//   - The goroutines which retrieve data can themselves have "parallel" logic, e.g. FirstFunctionWins
//   - All goroutines writing to these channels should respect `ctx.Done()`. This is why initialState()
//     is passed the context.
func RunStateMachine4[T1 any, T2 any, T3 any, T4 any, T5 any, T6 any, T7 any, T8 any, O any](
	ctx context.Context,
	initialState func(context.Context) (
		<-chan WithError[T1], <-chan WithError[T2], <-chan WithError[T3], <-chan WithError[T4],
		<-chan WithError[T5], <-chan WithError[T6], <-chan WithError[T7], <-chan WithError[T8],
	),
	processState func(context.Context,
		StateWrapper[T1], StateWrapper[T2], StateWrapper[T3], StateWrapper[T4],
		StateWrapper[T5], StateWrapper[T6], StateWrapper[T7], StateWrapper[T8],
	) (bool, O, error)) (output O, err error) {
	ctx, cancel := context.WithCancel(ctx)

	// The caller provides a callback which may provide channels -- but based on the supplied ctx
	// Note that `nil` is also perfectly fine.
	c1, c2, c3, c4, c5, c6, c7, c8 := initialState(ctx)

	// Internal state
	state1, state2, state3, state4, state5, state6, state7, state8 := startState[T1](c1), startState[T2](c2), startState[T3](c3), startState[T4](c4), startState[T5](c5), startState[T6](c6), startState[T7](c7), startState[T8](c8)

	// State visible to processor
	wrapper1, wrapper2, wrapper3, wrapper4, wrapper5, wrapper6, wrapper7, wrapper8 := makeWrapper[T1](&state1), makeWrapper[T2](&state2), makeWrapper[T3](&state3), makeWrapper[T4](&state4), makeWrapper[T5](&state5), makeWrapper[T6](&state6), makeWrapper[T7](&state7), makeWrapper[T8](&state8)

	for {
		select {
		case withError, ok := <-state1.receiveChannel:
			acceptData(&state1, withError, ok)
		case withError, ok := <-state2.receiveChannel:
			acceptData(&state2, withError, ok)
		case withError, ok := <-state3.receiveChannel:
			acceptData(&state3, withError, ok)
		case withError, ok := <-state4.receiveChannel:
			acceptData(&state4, withError, ok)
		case withError, ok := <-state5.receiveChannel:
			acceptData(&state5, withError, ok)
		case withError, ok := <-state6.receiveChannel:
			acceptData(&state6, withError, ok)
		case withError, ok := <-state7.receiveChannel:
			acceptData(&state7, withError, ok)
		case withError, ok := <-state8.receiveChannel:
			acceptData(&state8, withError, ok)
		}

		done, result, err := processState(ctx, wrapper1, wrapper2, wrapper3, wrapper4, wrapper5, wrapper6, wrapper7, wrapper8)
		if done {
			cancel()
			return result, err
		}
	}
}
