// Package healthz knows about worker queues and computes /healthz based on whether the work queues are full or not
package healthz
