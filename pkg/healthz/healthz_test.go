package healthz

import (
	"testing"
)

func TestChannelLength(t *testing.T) {
	c := make(chan bool, 10)

	if expected := 0; len(c) != expected {
		t.Errorf("Length is %d, expected %d", len(c), expected)
	}

	c <- false

	if expected := 1; len(c) != expected {
		t.Errorf("Length is %d, expected %d", len(c), expected)
	}

	c <- false
	if expected := 2; len(c) != expected {
		t.Errorf("Length is %d, expected %d", len(c), expected)
	}

	<-c
	if expected := 1; len(c) != expected {
		t.Errorf("Length is %d, expected %d", len(c), expected)
	}
}
