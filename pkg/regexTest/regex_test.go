package regexTest

import (
	"cmp"
	"fmt"
	"regexp"
	"slices"
	"strings"
	"testing"
	"time"

	excelize "github.com/xuri/excelize/v2"
)

const inputCount = 100

type RegexInfo struct {
	idx               int
	sum               int64
	count             int64
	text              string
	compiled          *regexp.Regexp
	lowercaseCompiled *regexp.Regexp
}

func (regexInfo *RegexInfo) matches(text string) bool {
	if regexInfo.lowercaseCompiled != nil {
		return regexInfo.lowercaseCompiled.Match([]byte(strings.ToLower(text))) // && regexInfo.compiled.Match([]byte(text))
	} else {
		return regexInfo.compiled.Match([]byte(text))
	}
}

func makeRegexs(t testing.TB) (regexs []RegexInfo) {

	f, err := excelize.OpenFile("ActiveRegexPolicies_20240802.xlsx")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	cols, err := f.Cols("Sheet1")
	if err != nil {
		t.Fatal("Cols", err)
	}
	cols.Next()
	rows, err := cols.Rows()
	if err != nil {
		t.Fatal("Rows", err)
	}
	for _, cell := range rows {
		compiled, err := regexp.Compile(cell)
		if err == nil {
			if strings.HasPrefix(cell, "(?i)") {
				regexs = append(regexs, RegexInfo{
					compiled:          compiled,
					text:              cell,
					lowercaseCompiled: regexp.MustCompile(strings.ToLower(strings.TrimPrefix(cell, "(?i)"))),
				})
			} else {
				regexs = append(regexs, RegexInfo{
					compiled: compiled,
					text:     cell,
				})
			}
		}
	}

	// for i := range numRegexs {
	// regexs = append(regexs, *regexp.MustCompile(fmt.Sprintf("This is a regex %d with suffix", i)))
	// }

	return
}

func makeInputs(t testing.TB) (inputs []string) {
	f, err := excelize.OpenFile("inputs.xlsx")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	cols, err := f.Cols("Sheet1")
	if err != nil {
		t.Fatal("Cols", err)
	}
	cols.Next()
	rows, err := cols.Rows()
	if err != nil {
		t.Fatal("Rows", err)
	}
	for _, cell := range rows {
		if err == nil {
			inputs = append(inputs, cell)
		}
	}

	inputs = inputs[:inputCount]
	return
}

func BenchmarkInnerLoopInputs(b *testing.B) {
	regexs := makeRegexs(b)
	inputs := makeInputs(b)
	b.ResetTimer()

	count := 0
	for range b.N {
		for regexIdx, regex := range regexs {
			for _, input := range inputs {
				before := time.Now()
				if regex.matches(input) {
					count++
				}
				regexs[regexIdx].count++
				regexs[regexIdx].sum += (time.Since(before).Nanoseconds())
			}
		}
	}
	slices.SortFunc(regexs, func(p1, p2 RegexInfo) int {
		return cmp.Compare(p2.sum, p1.sum)
	})

	fmt.Println()
	fmt.Println("Count", count/b.N)
}

func BenchmarkInnerLoopRegexs(b *testing.B) {
	regexs := makeRegexs(b)
	inputs := makeInputs(b)
	b.ResetTimer()

	count := 0
	for range b.N {
		for _, input := range inputs {
			for regexIdx, regex := range regexs {
				before := time.Now()
				if regex.matches(input) {
					count++
				}
				regexs[regexIdx].count++
				regexs[regexIdx].sum += (time.Since(before).Nanoseconds())
			}
		}
	}

	slices.SortFunc(regexs, func(p1, p2 RegexInfo) int {
		return cmp.Compare(p2.sum, p1.sum)
	})

	if false {
		for idx := range regexs {
			fmt.Println(idx, regexs[idx].sum/regexs[idx].count, regexs[idx].idx, regexs[idx].text)
		}
	}
	fmt.Println()
	fmt.Println("Count", count/b.N)
}
