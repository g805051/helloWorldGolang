package closure

import (
	"math"
	"sync"
	"testing"
)

func CreateCounter() func() int {
	counter := 0

	return func() int {
		counter++
		return counter
	}
}

func Test1(t *testing.T) {
	counter1 := CreateCounter()
	counter2 := CreateCounter()

	sequence := []struct {
		expected int
		current  int
		text     string
	}{
		{1, counter1(), "counter1"},
		{2, counter1(), "counter1"},
		{3, counter1(), "counter1"},
		{1, counter2(), "counter2"},
		{2, counter2(), "counter2"},
		{4, counter1(), "counter1"},
		{5, counter1(), "counter1"},
		{3, counter2(), "counter2"},
	}

	for _, step := range sequence {
		if step.expected != step.current {
			t.Error("For ", step.text, " expected ", step.expected, " but got ", step.current)
		}
	}
}

func BenchmarkParam(b *testing.B) {
	f := func(a float64) float64 {
		return math.Sin(math.Cos(a))
	}
	sum := 0.0

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		sum += f(float64(i * i))
	}
}

func BenchmarkConstParam(b *testing.B) {
	f := func(a float64) float64 {
		return math.Sin(math.Cos(a))
	}
	sum := 0.0

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		sum += f(77.0)
	}
}

func BenchmarkClosure(b *testing.B) {
	f := func(a func() float64) float64 {
		return math.Sin(math.Cos(a()))
	}
	sum := 0.0

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		input := func() float64 { return float64(i * i) }
		sum += f(input)
	}
}
func BenchmarkConstClosure(b *testing.B) {
	f := func(a func() float64) float64 {
		return math.Sin(math.Cos(a()))
	}
	sum := 0.0

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		input := func() float64 { return 77.0 }
		sum += f(input)
	}
}

func BenchmarkClosures(b *testing.B) {
	f := func(a func() float64) float64 {
		return math.Sin(math.Cos(a()))
	}
	sum := 0.0

	b.ResetTimer()

	var input func() float64

	for i := 0; i < b.N; i++ {
		if i%2 == 0 {
			input = func() float64 { return float64(i * i) }
		} else {
			input = func() float64 { return float64(-i * i) }
		}
		sum += f(input)
	}
}

func BenchmarkCallback(b *testing.B) {
	sum := 0.0

	accept := func(a float64) {
		sum += a
	}

	f := func(x float64, a func(float64)) {
		a(math.Sin(math.Cos(x)))
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		f(float64(i*i), accept)
	}
}

func BenchmarkChannel0(b *testing.B) {
	f := func(a float64) float64 {
		return math.Sin(math.Cos(a))
	}
	c := make(chan float64, 0)
	var wg sync.WaitGroup
	wg.Add(1)

	sum := 0.0
	go func() {
		for x := range c {
			sum += x
		}
		wg.Done()
	}()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		c <- f(float64(i * i))
	}
	close(c)
	wg.Wait()
}

func BenchmarkChannel256(b *testing.B) {
	f := func(a float64) float64 {
		return math.Sin(math.Cos(a))
	}
	c := make(chan float64, 256)
	var wg sync.WaitGroup
	wg.Add(1)

	sum := 0.0
	go func() {
		for x := range c {
			sum += x
		}
		wg.Done()
	}()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		c <- f(float64(i * i))
	}
	close(c)
	wg.Wait()
}
func BenchmarkChannelParam0(b *testing.B) {
	f := func(c *chan float64, a float64) {
		*c <- math.Sin(math.Cos(a))
	}
	c := make(chan float64, 0)
	var wg sync.WaitGroup
	wg.Add(1)

	sum := 0.0
	go func() {
		for x := range c {
			sum += x
		}
		wg.Done()
	}()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		f(&c, float64(i*i))
	}
	close(c)
	wg.Wait()
}

func BenchmarkChannelParam256(b *testing.B) {
	f := func(c *chan float64, a float64) {
		*c <- math.Sin(math.Cos(a))
	}
	c := make(chan float64, 256)
	var wg sync.WaitGroup
	wg.Add(1)

	sum := 0.0
	go func() {
		for x := range c {
			sum += x
		}
		wg.Done()
	}()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		f(&c, float64(i*i))
	}
	close(c)
	wg.Wait()
}
